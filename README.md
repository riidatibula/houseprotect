A simple web application that does the following:

Accept 2 inputs: 
1. A CSV file of target client information (including name, phone number, and city)
2. A date

Display a table of results that displays information about the client:
1. Name
2. Phone number
3. City
4. Weather at that city on the given date
