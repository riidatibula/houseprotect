import csv, io, requests

from django.shortcuts import render
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from houseprotect import settings

# Create your views here.

def index(request):
	return render(request, 'weatherchecker/index.html', {})


def check(request):
	customer_csv = request.FILES['file']
	date = request.POST['date']
	results = []
	api_key = 'key=' + settings.WEATHER_API_KEY
	api_date_request = '&dt=' + date + '&q='
	request_api = "https://api.weatherapi.com/v1/history.json?" + api_key + api_date_request

	if not customer_csv.name.endswith('.csv'):
		messages.error(request, 'Please upload only csv file')
		return HttpResponseRedirect(reverse('weatherchecker:index'))


	data_set = customer_csv.read().decode('UTF-8')
	io_string = io.StringIO(data_set)
	next(io_string)

	for row in csv.reader(io_string, delimiter=','):
		weather = 'Cannot be determined'
		response = requests.get(request_api + row[6])

		if response.status_code == 200:
			condition = response.json()['forecast']['forecastday'][0]['day']['condition']
			weather = condition['text']

		customer = {
			'name':  row[1] + ' ' + row[2] + ' ' + row[3],
			'phone_number': row[8],
			'address': row[6],
			'weather': weather
		}
		results.append(customer)

	context = {
		'results': results,
		'date': date
	}
	return render(request, 'weatherchecker/results.html', context=context)
